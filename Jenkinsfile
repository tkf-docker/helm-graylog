pipeline {
  agent any;

  options {
    buildDiscarder(logRotator(numToKeepStr: '6'))
  }

  stages {
    stage('Setup Environment') {
      steps {
        script {
          env.EXIT_STATUS = ''
          env.CURR_DATE = sh(
            script: '''date '+%Y-%m-%d_%H:%M:%S%:z' ''',
            returnStdout: true).trim()
          env.GITHASH_SHORT = sh(
            script: '''git log -1 --format=%h''',
            returnStdout: true).trim()
          env.GITHASH_LONG = sh(
            script: '''git log -1 --format=%H''',
            returnStdout: true).trim()
          env.GIT_TAG_NAME = sh(
            script: '''git describe --tags ${commit}''',
            returnStdout: true).trim()
          currentBuild.displayName = "${GIT_TAG_NAME}"
        }
      }
    }
    stage('Update Helm Charts') {
      steps {
        script {
          sh '''
            # Update the Chart Version with the tag from git
            sed -i 's/version:.*/version: '"$GIT_TAG_NAME"'/g' tkf-mongodb/Chart.yaml
            helm lint tkf-mongodb
            helm package tkf-mongodb
            helm push-artifactory tkf-mongodb-${GIT_TAG_NAME}.tgz teknofilea

            sed -i 's/version:.*/version: '"$GIT_TAG_NAME"'/g' tkf-opensearch/Chart.yaml
            helm lint tkf-opensearch
            helm package tkf-opensearch
            helm push-artifactory tkf-opensearch-${GIT_TAG_NAME}.tgz teknofilea

            sed -i 's/version:.*/version: '"$GIT_TAG_NAME"'/g' tkf-graylog/Chart.yaml
            helm lint tkf-graylog
            helm package tkf-graylog
            helm push-artifactory tkf-graylog-${GIT_TAG_NAME}.tgz teknofilea
          '''
        }
      }
    }
  }
  post {
    always {
      echo 'Cleaning up.'
      deleteDir() /* clean up our workspace */
    }
  }
}
